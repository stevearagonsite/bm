package Ambient{
	
	import flash.display.BlendMode;
	import flash.display.Sprite;

	public class Background{
		
		public var backFx:Sprite = new Sprite;//Here i save my backgrounds.
		public var model1:MClevelBack1 = new MClevelBack1;//Is my background.
		public var model2:MClevelBack1 = new MClevelBack1;//Is my background.
		public var modelMain:MCambient1 = new MCambient1;//This is the color FXs.
		private static var speedBackground:int = 2;//Is value for move the background.
		
		public function set Speed(value:int):void{speedBackground = value;}
		
		public function spawn():void{
			
			//Push in the stage my movieclips.
			Main.mainStage.addChild(backFx);
			//The all movieclips are childs of the backFX.
			backFx.addChild(model1);
			backFx.addChild(model2);
			backFx.addChild(modelMain);
			
			model2.y = -model2.height;
			model1.x = model2.x = modelMain.x = Main.mainStage.stageWidth/2;
			modelMain.y = Main.mainStage.stageHeight/2;
			
			modelMain.blendMode = BlendMode.LIGHTEN;//Here create the FX color for the space.
		}
		
		/**Is the main Update of the class.**/
		public function evUpdate():void{
			//Here i create FX move.
			if (model1 != null || model2 != null){		
				model1.y += speedBackground;
				model2.y += speedBackground;
				//Control the position.
				if (model1.y >= Main.mainStage.stageHeight) model1.y = model2.y-model1.height;
				if (model2.y >= Main.mainStage.stageHeight) model2.y = model1.y-model2.height;
			}
		}
		
		/**I can to destroy the background events and movieclips.**/
		public function evDestroyed():void{
			
			if (backFx != null){
				
				backFx.removeChild(model1);
				backFx.removeChild(model2);
				backFx.removeChild(modelMain);
				
				model1 = null;
				model2 = null;
				modelMain = null;
				
				Main.mainStage.removeChild(backFx);
				backFx = null;
			}
		}
	}
}