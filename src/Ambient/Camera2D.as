package Ambient{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;

	public class Camera2D{
		
		public var view:Sprite = new Sprite();
		public var targetZoom:Number;
		public var delayZoom:Number = 20;
		
		public function Camera2D(){
			trace("Ready cam...");
		}
		
		public function on():void{
			trace("On camera.");
			Main.mainStage.addChild(view);
		}
		
		public function off():void{
			trace("Turn off camera.");
			Main.mainStage.removeChild(view);
		}
		
		/**I can child for the fx for the camera.**/
		public function addToView(obj:Sprite):void{
			view.addChild(obj);
		}
		/**I can to remove child for the fx camera.**/
		public function removeFromView(obj:Sprite):void{
			view.removeChild(obj);
		}
		
		/**I can control the camera zoom.**/
		public function set zoom(value:Number):void{
			//trace("SET");
			if(value > 0){
				view.scaleX = view.scaleY = value;
			}else
				throw new Error("the zoom cant be < 0 ... " + "value: " + value);
		}
		
		/**Return to the value zoom.**/
		public function get zoom():Number{
			//trace("GET");
			return view.scaleX;
		}
		
		/**I create the FX smooth zoom**/
		public function set smoothZoom(value:Number):void{
			targetZoom = value;
			Main.mainStage.addEventListener(Event.ENTER_FRAME, evUpdateSmoothZoom);
		}
		
		/**I have the Update for the fx smooth zoom, this is call of the function smooth zoom.**/
		protected function evUpdateSmoothZoom(event:Event):void{
			
			if (Main.isMixerHeros == true){// I check the state of the heros.
				zoom += (targetZoom - zoom) / delayZoom;
				if(Math.abs((targetZoom - zoom)) <= 0.1){//Check the value of the targetZoom with currentZoom. 
					zoom = targetZoom;
					smoothZoom = 1;
					Main.mainStage.addEventListener(Event.ENTER_FRAME, evUpdateSmoothOut);
				}
			}
		}
		
		/**I have the Update for the fx smooth zoom, this is call of the function evUpdateSmoothZoom.**/
		protected function evUpdateSmoothOut(event:Event):void{
			
			if (Main.isMixerHeros == true){//Check the state heros.
				view.x -= view.x/10;
				view.y -= view.y/10;
				zoom += (targetZoom - zoom) / delayZoom;
				if (Main.mainStage.frameRate <= 60)//Create the FX delay.
					Main.mainStage.frameRate += Main.mainStage.frameRate/5;
				//Return the camera normal position.
				if(Math.abs((targetZoom - zoom)) <= 0.001 && Main.mainStage.frameRate >= 60){
					zoom = targetZoom;
					Main.mainStage.frameRate = 60;
					evDestroyedSmooth();
				}
			}
		}
		
		/**Destroy the old events of the FX smooth zoom.**/
		private function evDestroyedSmooth():void{
			Main.mainStage.removeEventListener(Event.ENTER_FRAME, evUpdateSmoothZoom);
			Main.mainStage.removeEventListener(Event.ENTER_FRAME, evUpdateSmoothOut);
			
			Main.isBlockSpace = false;
		}
		
		public function set x(value:Number):void{
			view.x = -value;
		}
		
		public function get x():Number{
			return -view.x;
		}
		
		public function set y(value:Number):void{
			view.y = -value;
		}
		
		public function get y():Number{
			return -view.y;
		}
		
		/**Target zoom in the stage.**/
		public function lookAt(target:Sprite):void{
			
			var pLocal:Point = new Point(target.x, target.y);
			var pGlobal:Point = target.parent.localToGlobal(pLocal);
			var pView:Point = view.globalToLocal(pGlobal);
			
			x = pView.x * zoom - Main.mainStage.stageWidth/2;
			y = pView.y * zoom - Main.mainStage.stageHeight/2;
		}
	}
}