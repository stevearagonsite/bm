package{
	
	import com.greensock.TweenMax;

	public class Enemy{
		
		public var model:MCenemy;//It is my model.
		public var isDestroyed:Boolean = false;//To be ??
		public var isIce:Boolean = false;//Ice state.
		public var timeToIce:int = 2500;//Time for remove ice.
		public var currentTimeToIce:Number = timeToIce;//Current time to remove ice.
		private var currentSpeed:int;//Set this value when this is state ice.
		private const maxSpeed:uint = 6;//This is the maximum speed (for random). 
		private const minSpeed:uint = 3;//This is the minimun speed (for random).
		public var speed:int;//This is the speed for element
		public var life:int;//Loading....(when this is call).
		public var scale:Number//loading....(when this is call).
		public var damage:int;//Loading....(when this is call).
		
		/**This is the parameters initals.**/
		public function Spawn(positionX:int, scale:Number, life:int, damage:int):void{
			this.life = life;
			this.scale = scale;
			this.damage = damage;
			
			model = new MCenemy;
			Main.layer1.addChild(model);
			model.trigger.visible = false;
			
			model.x = positionX;
			model.y = -model.height;
			
			model.scaleX = model.scaleY = scale;
			
			speed = Main.instace.getRandom(minSpeed , maxSpeed);//is the speed for element
			TweenMax.to(model, 15, {motionBlur:{strength:speed/10, quality:1}});//this is filter animation in the asteroids, parameters by speed
		}
		
		/**Deleted life for my enemy.(Call others class)**/
		public function evRemoverLife():void{
			life -= 1;
		}
		
		/**Update execute in the main.**/
		public function evUpdate():void{
			if (model != null)model.y += speed;//This is move for my absteroid
			if (isIce)EvTimerIce();//When absteroid is ice, active timer for remove ice.
			evDestroyed();
		}
		
		/**When is active the power ice**/
		public function EvIce():void{
			if (model != null){
				isIce = true;
				evPause();
				TweenMax.to(model, 1, {colorMatrixFilter:{colorize:0x0000ff, amount:0.6}});
				currentSpeed = speed;
				speed = 2;
			}
		}
		
		/**When is active the power ice**/
		private function EvRemoveIce():void{
			if (model != null){
				isIce = false;
				TweenMax.to(model, 1, {colorMatrixFilter:{colorize:0x0000ff, amount:0}});
				evRemovePause();
				speed = currentSpeed;
			}
		}
		
		/**Active when this is Ice**/
		private function EvTimerIce():void{
			currentTimeToIce -= 1000/Main.mainStage.frameRate;
			if (currentTimeToIce < 0){
				currentTimeToIce = timeToIce;
				EvRemoveIce();
			}
		}
		
		/**Execute pause in the animation**/
		public function evPause():void{
			if (!isDestroyed){
				var i:int = model.currentFrame;
				model.gotoAndStop(i);
			}
		}
		
		/**This is the resume for animation.**/
		public function evRemovePause():void{
			if (!isDestroyed){
				var i:int = model.currentFrame;
				model.gotoAndPlay(i);
			}
		}
		
		/**Destroy for contiditions**/
		public function evDestroyed():void{
			if (model != null && model.y >= Main.mainStage.stageHeight + model.height || life <= 0){
				if (life <= 0 && scale == 0.5){Main.instace.evExplotion(model.x,model.y, 0.5);
				}else if (life <= 0 && scale == 1)Main.instace.evExplotion(model.x,model.y, 1);

				Main.layer1.removeChild(model);
				model = null;
				isDestroyed = true;
			}
		}
		
		/****/
		public function evDestroyedOut():void//here deleted my movieclips for stage and dispatch with boolean destroyed
		{
			if (model != null){
				Main.layer1.removeChild(model);
				model = null;
				isDestroyed = true;
			}
		}
	}
}