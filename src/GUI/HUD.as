package GUI{

	public class HUD{
		
		public var model:MChud;//This is the movieclip that contains HUD.
		public var timeActive:Boolean = false;//Is the state for the time.
		public var timeWin:Boolean = false;//Is the conclusion time.
		public var timeToWin:int = 15000;//Time for win the level
		public var currentTimeToWin:int = timeToWin;//Time default.
		
		public function HUD(){
			trace("Ready HUD...");
			
			//Spawn HUD.
			model = new MChud;
			Main.mainStage.addChild(model);
			//Set position HUD.
			model.x = Main.mainStage.stageWidth/2;
			model.y = Main.mainStage.stageHeight/6;
		}
		
		/**This is the state for the HUD.**/
		public function evVisible():void{
			if (model != null && !model.visible){model.visible = true;
			}else if (model != null && model.visible)model.visible = false;
		}
		
		/**This is the state for the hero number 1.**/
		public function hero1():void{
			
			if (model != null){
				var x:int = 100-Main.life1;
				model.hero1.mc_life2.gotoAndStop(x);
				model.hero1.mc_life1.gotoAndStop(x);
				
				if (!Main.isMixerHeros){
					model.hero1.mc_life2.visible = false;
					model.hero1.mc_life1.visible = true;
				}else{						
					model.hero1.mc_life2.visible = true;
					model.hero1.mc_life1.visible = false;
				}
			}
		}
		
		/**This is the state for the hero number 2.**/
		public function hero2():void{
			
			if (model != null){
				var x:int = 100-Main.life2;
				model.hero2.mc_life2.gotoAndStop(x);
				model.hero2.mc_life1.gotoAndStop(x);
				
				if (!Main.isMixerHeros){
					model.hero2.mc_life2.visible = false;
					model.hero2.mc_life1.visible = true;
				}else{						
					model.hero2.mc_life2.visible = true;
					model.hero2.mc_life1.visible = false;
				}
			}
		}
		
		/**This is the state for the super hero.**/
		public function superHero():void{
			
			if (model != null){
				var x:int = 100-Main.superLife;
				model.superHero.mc_life.gotoAndStop(x);
			}
		}
		
		/**This is the state for the points.**/
		public function score():void{
			
			if (Main.goal < 10){
				model.mc_score.tb_score.text = "0"+Main.totalPoints+"/0"+Main.goal;
			}else if (Main.totalPoints < 10){
				model.mc_score.tb_score.text = "0"+Main.totalPoints+"/"+Main.goal;
			}else
				model.mc_score.tb_score.text = ""+Main.totalPoints+"/"+Main.goal;
		}
		
		/**This is the state for the conclusion time.**/
		public function timeFinished():void{
			if (timeActive == true && model != null){
				
				currentTimeToWin -= 1000 / Main.mainStage.frameRate;//here i can control to my spawn asteriods2
				if(currentTimeToWin <= 0){
					currentTimeToWin = timeToWin;
					timeWin = true;
				}
				
				var min:int = (currentTimeToWin/1000)/60;
				var seg:int = (currentTimeToWin - min * 60000)/1000;
				
				if (min >= 1){
					model.mc_score.tb_time.text ="0"+min+":"+seg;
					if (seg < 10)
						model.mc_score.tb_time.text = "0"+min+":0"+seg;
				}else if (min < 1){
					model.mc_score.tb_time.text = "00"+":"+seg;
					if (seg < 10)
						model.mc_score.tb_time.text = "0"+min+":0"+seg;
				}
			}
		}
		
		/**This is Update of the class.**/
		public function evUpdate():void{
			
			timeFinished();
			score();
			hero1();
			hero2();
			superHero();
		}
		
		/**Destroy all assets and var of the class.**/
		public function evDestroyed():void{
			if (model != null){
				Main.mainStage.removeChild(model);
				model = null;
			}
		}
	}
}