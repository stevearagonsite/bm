package GUI{
	
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	public class Menu{
		
		public var mainMenu:MCmenu;//This is the movieclip for menu.
		public var howToPlay:MChowToPlay;//This is the movieclip for how to play.
		public var credits:MCcredits;//This is the movieclip for menu.
		public var howToPlayScreen:uint;//I can to know current page of the how to play.
		
		public var selectionNow:String;//Current selection of the menu.
		public const selectionPlay:String = "play";//Type selection for the selectionNow.
		public const selectionHowToPlay:String = "howToPlay";//Type selection for the selectionNow.
		public const selectionCredits:String = "credits";//Type selection for the selectionNow.
		public const selection:String = "2";//Type selection for the selectionNow.
		public const outSelection:String = "1";//Type selection for the selectionNow.
		
		/**I can spawn with intials parameters the menu.**/
		public function spawn():void{
			
			Main.audioBackground.EvPlayLoop();//Sound ambient active.
			Main.audioBackground.volume = 0.3;//Volume of the sound ambient.
	
			mainMenu = new MCmenu;//Here i create instance of the MCmenu.
			Main.mainStage.addChild(mainMenu);//Push in the stage instance of the MCmenu.
			
			//Initials paramets of the buttons.
			mainMenu.mc_play.gotoAndStop(outSelection);
			mainMenu.mc_howToPlay.gotoAndStop(outSelection);
			mainMenu.mc_credits.gotoAndStop(outSelection);
			
			mainMenu.mc_play.addEventListener(MouseEvent.MOUSE_OVER, evPlayOver);
			mainMenu.mc_howToPlay.addEventListener(MouseEvent.MOUSE_OVER, evHowToPlayOver);
			mainMenu.mc_credits.addEventListener(MouseEvent.MOUSE_OVER,evCreditsOver);
			
			mainMenu.mc_play.addEventListener(MouseEvent.MOUSE_DOWN, evPlayDown);
			mainMenu.mc_credits.addEventListener(MouseEvent.MOUSE_DOWN,evCreditsDown);
			mainMenu.mc_howToPlay.addEventListener(MouseEvent.MOUSE_DOWN, evHowToPlayDown);
			
			Main.mainStage.addEventListener(KeyboardEvent.KEY_DOWN, evSelection);//Change parameters with keyboard.
		}
		
		/**Here i can control the menu with keyboard.**/
		protected function evSelection(event:KeyboardEvent):void{
			
			switch(event.keyCode){
				//--------------------------here evPreviusSelection 
				case Keyboard.A:{
					evPreviusSelection();
					break;
				}
				case Keyboard.LEFT:{
					evPreviusSelection();
					break;
				}
				case Keyboard.W:{
					evPreviusSelection();
					break;
				}
					
				case Keyboard.UP:{
					evPreviusSelection();
					break;
				}
				//--------------------------here evNextSelection
				case Keyboard.D:{
					evNextSelection();
					break;
				}
				case Keyboard.RIGHT:{
					evNextSelection();
					break;
				}
				case Keyboard.S:{
					evNextSelection();
					break;
				}
					
				case Keyboard.DOWN:{
					evNextSelection();
					break;
				}
				//----------------------here go selection
				case Keyboard.ENTER:{
					evGoSelection();
					break;
				}
				//---------------------Other events.
				case Keyboard.ESCAPE:{//Here create button pause for my game.
					Main.mainStage.nativeWindow.x = Main.mainStage.nativeWindow.y = 0;
					break;
				}
			}
		}
		
		/**If the user selection one button, execute the opcion.**/
		private function evGoSelection():void{
			
			switch(selectionNow){
				case selectionPlay:{
					evRemove();
					Main.instace.evStartGame();
					break;
				}
				case selectionHowToPlay:{
					evRemove();
					evHowToPlay();
					break;
				}
				case selectionCredits:{
					evRemove();
					evCredits();
					break;
				}
			}
		}
		
		/**Here i can control the animation and selection for the users.**/
		private function evPreviusSelection():void{
			
			switch(selectionNow){
				case selectionPlay:{
					systemSelection(selectionCredits);
					break;
				}
				case selectionHowToPlay:{
					systemSelection(selectionPlay);
					break;
				}
				case selectionCredits:{
					systemSelection(selectionHowToPlay);
					break;
				}
				default:{
					systemSelection(selectionPlay);
					break;
				}
			}
		}
		
		/**Here i can control the animation and selection for the users**/
		private function evNextSelection():void{
			
			switch(selectionNow){
				case selectionPlay:{
					systemSelection(selectionHowToPlay);
					break;
				}
				case selectionHowToPlay:{
					systemSelection(selectionCredits);
					break;
				}
				case selectionCredits:{
					systemSelection(selectionPlay);
					break;
				}
				default:{
					systemSelection(selectionPlay);
					break;
				}
			}
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evCreditsOver(event:MouseEvent):void{
			systemSelection(selectionCredits);
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evHowToPlayOver(event:MouseEvent):void{
			systemSelection(selectionHowToPlay);
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evPlayOver(event:MouseEvent):void{
			systemSelection(selectionPlay);
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evCreditsDown(event:MouseEvent):void{
			evGoSelection();
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evHowToPlayDown(event:MouseEvent):void{
			evGoSelection();
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evPlayDown(event:MouseEvent):void{
			evGoSelection();
		}
		
		/**This is the feadback for user the selection button.**/
		public function systemSelection(name:String):void{
			selectionNow = name;
			
			switch (name){
				case selectionPlay:{
					mainMenu.mc_play.gotoAndStop(selection);
					
					mainMenu.mc_howToPlay.gotoAndStop(outSelection);
					mainMenu.mc_credits.gotoAndStop(outSelection);
					break;
				}
				case selectionHowToPlay:{
					mainMenu.mc_howToPlay.gotoAndStop(selection);
					
					mainMenu.mc_play.gotoAndStop(outSelection);
					mainMenu.mc_credits.gotoAndStop(outSelection);
					break;
				}
				case selectionCredits:{
					mainMenu.mc_credits.gotoAndStop(selection);
					
					mainMenu.mc_play.gotoAndStop(outSelection);
					mainMenu.mc_howToPlay.gotoAndStop(outSelection);
					break;
				}
			}
		}
		
		public function evRemove():void{
			
			if (mainMenu != null){
				mainMenu.mc_play.removeEventListener(MouseEvent.MOUSE_OVER, evPlayOver);
				mainMenu.mc_howToPlay.removeEventListener(MouseEvent.MOUSE_OVER, evHowToPlayOver);
				mainMenu.mc_credits.removeEventListener(MouseEvent.MOUSE_OVER,evCreditsOver);
				
				mainMenu.mc_play.removeEventListener(MouseEvent.MOUSE_DOWN, evPlayDown);
				mainMenu.mc_credits.removeEventListener(MouseEvent.MOUSE_DOWN,evCreditsDown);
				mainMenu.mc_howToPlay.removeEventListener(MouseEvent.MOUSE_DOWN, evHowToPlayDown);
				
				Main.mainStage.removeEventListener(KeyboardEvent.KEY_DOWN, evSelection)
				Main.mainStage.removeChild(mainMenu);
				mainMenu = null;
				Main.audioBackground.stop();
			}
			
			if (howToPlay != null){
				howToPlay.removeEventListener(MouseEvent.CLICK, evHowToPlayMouseNext);
				Main.mainStage.removeEventListener(KeyboardEvent.KEY_UP,evHowToPlayKey);
				
				Main.mainStage.removeChild(howToPlay);
				howToPlay = null;
				howToPlayScreen += 1;
			}
			
			if (credits != null){
				credits.removeEventListener(MouseEvent.CLICK, evCreditsMouse);
				Main.mainStage.removeEventListener(KeyboardEvent.KEY_UP, evCreditsKey);
				
				Main.mainStage.removeChild(credits);
				credits = null;
			}
		}
		
		private function evCredits():void{
			credits = new MCcredits;
			Main.mainStage.addChild(credits);
			
			credits.addEventListener(MouseEvent.CLICK, evCreditsMouse);
			Main.mainStage.addEventListener(KeyboardEvent.KEY_UP, evCreditsKey);
		}
		
		protected function evCreditsKey(event:KeyboardEvent):void{
			
			switch(event.keyCode){
				//--------------------------here evPreviusSelection 
				case Keyboard.A:{
					evCreditsExit();
					break;
				}
				case Keyboard.LEFT:{
					evCreditsExit();
					break;
				}
				case Keyboard.W:{
					
					evCreditsExit();
					break;
				}
					
				case Keyboard.UP:{
					evCreditsExit();
					break;
				}
					//--------------------------here evNextSelection
				case Keyboard.D:{
					evCreditsExit();
					break;
				}
				case Keyboard.RIGHT:{
					evCreditsExit();
					break;
				}
				case Keyboard.S:{
					
					evCreditsExit();
					break;
				}
					
				case Keyboard.DOWN:{
					evCreditsExit();
					break;
				}
					
			}
		}
		
		protected function evCreditsMouse(event:MouseEvent):void{evCreditsExit();}
		
		private function evCreditsExit():void{
			evRemove();
			spawn();
		}
		
		private function evHowToPlay():void{
			howToPlay = new MChowToPlay;
			Main.mainStage.addChild(howToPlay);
			
			howToPlayScreen = 1;
			
			howToPlay.gotoAndStop(howToPlayScreen);
			
			howToPlay.addEventListener(MouseEvent.CLICK, evHowToPlayMouseNext);
			Main.mainStage.addEventListener(KeyboardEvent.KEY_UP,evHowToPlayKey)	
		}
		
		/**Here i can control the menu with keyboard.**/
		protected function evHowToPlayKey(event:KeyboardEvent):void{
			
			switch(event.keyCode){
				//--------------------------here evPreviusSelection 
				case Keyboard.A:{
					evPrevHowToPlay();
					break;
				}
				case Keyboard.LEFT:{
					evPrevHowToPlay();
					break;
				}
				case Keyboard.W:{
					evPrevHowToPlay();
					break;
				}
					
				case Keyboard.UP:{
					evPrevHowToPlay();
					break;
				}
					//--------------------------here evNextSelection
				case Keyboard.D:{
					evNextHowToPlay();
					break;
				}
				case Keyboard.RIGHT:{
					evNextHowToPlay();
					break;
				}
				case Keyboard.S:{		
					evNextHowToPlay();
					break;
				}
				case Keyboard.DOWN:{
					evNextHowToPlay();
					break;
				}		
			}
		}
		
		/**I can control the page of the how to play option.**/
		private function evPrevHowToPlay():void{
			if (howToPlayScreen >= 1){
				howToPlayScreen -= 1;
				howToPlay.gotoAndStop(howToPlayScreen);
			}else howToPlay.gotoAndStop(howToPlayScreen);
		}
		
		/**I can control the page of the how to play option.**/
		private function evNextHowToPlay():void{
			if (howToPlayScreen <= howToPlay.totalFrames){
				howToPlay.gotoAndStop(howToPlayScreen);
			}else{
				howToPlayScreen = 1;
				evRemove();
				spawn();
			}
		}
		
		/**I can control the page of the how to play option with the mouse.**/
		protected function evHowToPlayMouseNext(event:MouseEvent):void{
			
			if (howToPlayScreen <= howToPlay.totalFrames){
				howToPlayScreen += 1;
				howToPlay.gotoAndStop(howToPlayScreen);
			}else{
				howToPlayScreen = 1;
				evRemove();
				spawn();
			}
		}
	}
}