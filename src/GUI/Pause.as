package GUI{
	import flash.desktop.NativeApplication;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.system.fscommand;
	import flash.ui.Keyboard;

	public class Pause{
		
		public var model:MCpause;//The model for menu.
		public const resume:String = "2";//Defult parameter for resume.
		public const menu:String = "3";//Defult parameter for menu.
		public const exit:String = "4";//Defult parameter for exit.
		public var x:int;//Value for options in this menu.
		
		public function Pause(){
			trace("Ready pause...");
		}
		
		/**This function i can painting this menu.**/
		public function Spawn():void{
			model = new MCpause;
			Main.mainStage.addChild(model);
			
			//This parameters i can check collission with mouse.
			model.triggerResume.alpha = 0;
 			model.triggerMenu.alpha = 0;
			model.triggerExit.alpha = 0;
			
			model.gotoAndStop(1);
			
			//Events for the mouse.
			model.triggerResume.addEventListener(MouseEvent.MOUSE_OVER, evSelectionResume);
			model.triggerMenu.addEventListener(MouseEvent.MOUSE_OVER, evSelectionMenu);
			model.triggerExit.addEventListener(MouseEvent.MOUSE_OVER, evSelectionExit);
			
			model.triggerResume.addEventListener(MouseEvent.MOUSE_DOWN, evDownResume);
			model.triggerMenu.addEventListener(MouseEvent.MOUSE_DOWN, evDownMenu);
			model.triggerExit.addEventListener(MouseEvent.MOUSE_DOWN, evDownExit);
			
			Main.mainStage.addEventListener(KeyboardEvent.KEY_DOWN, evSelectionKey);
		}

		/**Here is the event of the mouse for the selection buttons.**/
		protected function evDownExit(event:MouseEvent):void{
			evExit();
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evDownMenu(event:MouseEvent):void{
			Main.audioBackground2.stop();
			evMenu();
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evDownResume(event:MouseEvent):void{
			evResume();
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evSelectionExit(event:MouseEvent):void{
			model.gotoAndStop(exit);
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evSelectionMenu(event:MouseEvent):void{
			model.gotoAndStop(menu);	
		}
		
		/**Here is the event of the mouse for the selection buttons.**/
		protected function evSelectionResume(event:MouseEvent):void{
			model.gotoAndStop(resume);
		}
		
		/**Here i can control the menu with keyboard.**/
		protected function evSelectionKey(event:KeyboardEvent):void{
			switch(event.keyCode){
				//--------------------------here evPreviusSelection 
				case Keyboard.A:{
					evPreviusSelection();
					break;
				}
				case Keyboard.LEFT:{
					evPreviusSelection();
					break;
				}
				case Keyboard.W:{
					evPreviusSelection();
					break;
				}
					
				case Keyboard.UP:{
					evPreviusSelection();
					break;
				}
					//--------------------------here evNextSelection
				case Keyboard.D:{
					evNextSelection();
					break;
				}
				case Keyboard.RIGHT:{
					evNextSelection();
					break;
				}
				case Keyboard.S:{
					evNextSelection();
					break;
				}
					
				case Keyboard.DOWN:{
					evNextSelection();
					break;
				}
					//----------------------here go selection
				case Keyboard.ENTER:{
					evGoSelection();
					break;
				}
					
			}
		}
		
		/**Here i can control the animation and selection for the users.**/
		private function evPreviusSelection():void{
			if (model != null){
				x = model.currentFrame;
				switch(x){
					case 2:{
						model.gotoAndStop(exit);
						break;
					}
					case 4:{
						model.gotoAndStop(menu);
						break;
					}
					case 3:{
						model.gotoAndStop(resume);
						break;
					}
					default:{
						if (model.currentFrame == 1)
							model.gotoAndStop(resume);
						break;
					}
				}
			}	
		}
		
		/**Here i can control the animation and selection for the users.**/
		private function evNextSelection():void{
			if (model != null){
				x = model.currentFrame;
				switch(x){
					case 2:{
						model.gotoAndStop(menu);
						break;
					}
					case 4:{
						model.gotoAndStop(resume);
						break;
					}
					case 3:{
						model.gotoAndStop(exit);
						break;
					}
					default:{
						if (model.currentFrame == 1)
						{
							model.gotoAndStop(resume);
						}
						break;
					}
				}
			}
		}
		
		/**If the user selection one button, execute the opcion.**/
		private function evGoSelection():void{
			if (model != null){
				var x:int = model.currentFrame;
				switch(x){
					case 2:{
						evResume();
						break;
					}
					case 3:{
						evMenu();
						break;
					}
					case 4:{
						evExit();
						break;
					}
				}
			}
		}
		
		/**The user can get out fot this game.**/
		private function evExit():void{
			evRemove();
			NativeApplication.nativeApplication.exit(); 
			fscommand("quit");
		}
		
		/**The user can get out for this menu and level.**/
		private function evMenu():void{
			evRemove();
			Main.instace.evRemoveAll();
			Main.instace.evResetDefultValue();
			Main.instace.evMenu();
		}
		
		/**Call function in classs Main for resume.**/
		private function evResume():void{
			Main.instace.removePause();
		}
		
		/**Here i can remove all assets for this class.**/
		public function evRemove():void{
			if (model != null){
				model.triggerResume.removeEventListener(MouseEvent.MOUSE_OVER, evSelectionResume);
				model.triggerMenu.removeEventListener(MouseEvent.MOUSE_OVER, evSelectionMenu);
				model.triggerExit.removeEventListener(MouseEvent.MOUSE_OVER, evSelectionExit);
				
				model.triggerResume.removeEventListener(MouseEvent.MOUSE_DOWN, evDownResume);
				model.triggerMenu.removeEventListener(MouseEvent.MOUSE_DOWN, evDownMenu);
				model.triggerExit.removeEventListener(MouseEvent.MOUSE_DOWN, evDownExit);
				
				Main.mainStage.removeChild(model);
				model = null;
			}
		}
	}
}