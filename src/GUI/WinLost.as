package GUI{
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;

	public class WinLost{
		
		public var win:MCwin;//This movieclip have win screen.
		public var gameOver:MCgameOver;//This movieclip hace Game Over screen.

		public function WinLost(){
			trace("Win or Lost ready...");
		}
		
		/**here execute the events when level is full**/
		public function evWin():void{
			
			Main.audioBackground2.volume = 0.1;//Here execute in the class amount of volume(SoundController).
			Main.instace.evRemoveAll();//Here execute in the Main remove all in the screen.
			win = new MCwin;//Create the movieclip in the screen for this menu.
			Main.mainStage.addChild(win);//Painting the movieclip in the stage.
			Main.audioWin.play();//This is the sound for the win screen.
			
			if (Main.currentLevel <= 9){//When in the screen the mission is 0#.		
				win.tb_level.text = "0"+Main.currentLevel;
			}else//This in the screen the mission is > 10.
				win.tb_level.text = ""+Main.currentLevel;
			
			win.gotoAndStop(1);//Initial parameter for this menu.
			Main.mainStage.addEventListener(Event.ENTER_FRAME, evUpdate);//Here generate feadback rotation in the screen.
			win.addEventListener(MouseEvent.CLICK, evMouseNextLevel);//This is event for the next level.
			Main.mainStage.addEventListener(KeyboardEvent.KEY_DOWN, evKeyNextLevel);//This is event for the next level.
		}
		
		/**Here generate a feadback for menu Win and this is the update for this class.**/
		protected function evUpdate(event:Event):void{
			if (win != null)
				win.mc_rotation.rotation += 5;//Rotation a moviclip child for feadback.
		}
		
		/**Here execute the event for exit the screen with (Enter).**/
		protected function evKeyNextLevel(event:KeyboardEvent):void{
			if (event.keyCode == Keyboard.ENTER)
				evNextLevelWin();	
		}
		
		/**Here execute the event for exit the screen with (Click).**/
		protected function evMouseNextLevel(event:MouseEvent):void{evNextLevelWin();}
		
		/**Here execute new level and deleted the win screen.**/
		private function evNextLevelWin():void{
			evDestroyedWin();//Deleted screen to win.
			
			Main.instace.removeEventListener(Event.ENTER_FRAME, evUpdate);//Remove generate feadback rotation in the screen.
			Main.instace.evNextLevel();
			Main.audioBackground2.stop();
			Main.instace.evStartGame();
		}
		
		private function evNextLevel():void{
			if (win != null){
				win.removeEventListener(MouseEvent.CLICK, evMouseNextLevel);
				Main.mainStage.removeEventListener(KeyboardEvent.KEY_DOWN, evKeyNextLevel);
			}
		}
		
		public function evGameOver():void{
			gameOver = new MCgameOver;//Here create the movieclip for the screen Game Over.
			Main.mainStage.addChild(gameOver);
			
			Main.audioBackground2.stop();
			Main.audioGameOver.play();
			
			gameOver.gotoAndStop(1);
			
			gameOver.tiggerNo.alpha = 0;
			gameOver.tiggerYes.alpha = 0;
			
			//------Here create the events for the menu Game Over.
			//Only events mouse.
			gameOver.tiggerYes.addEventListener(MouseEvent.MOUSE_OVER ,evYesOver);
			gameOver.tiggerNo.addEventListener(MouseEvent.MOUSE_OVER ,evNoOver);
			
			gameOver.tiggerYes.addEventListener(MouseEvent.MOUSE_DOWN ,evYesGo);
			gameOver.tiggerNo.addEventListener(MouseEvent.MOUSE_DOWN ,evNoGo);
			
			//Only events Keyboard.
			Main.mainStage.addEventListener(KeyboardEvent.KEY_DOWN ,selectionKey);
		}
		
		/**keyboard events for the screen Game Over**/
		protected function selectionKey(event:KeyboardEvent):void{
			if (gameOver != null){
				switch(event.keyCode){
					//--------------------------here evPreviusSelection 
					case Keyboard.A:{
						evNextSelection();
						break;
					}
					case Keyboard.LEFT:{
						evNextSelection();
						break;
					}
						
					case Keyboard.W:{
						evNextSelection();
						break;
					}
						
					case Keyboard.UP:{
						evNextSelection();
						break;
					}
						//--------------------------here evNextSelection
					case Keyboard.D:{
						evNextSelection();
						break;
					}
						
					case Keyboard.RIGHT:{
						evNextSelection();
						break;
					}
						
					case Keyboard.S:{
						evNextSelection();
						break;
					}
						
					case Keyboard.DOWN:{
						evNextSelection();
						break;
					}
						//----------------------here go selection
					case Keyboard.ENTER:{
						evGoSelection();
						break;
					}	
				}
			}
		}
		
		/**Change the selection for the keyboard**/
		private function evNextSelection():void{
			if (gameOver != null){
				if (gameOver.currentFrame == 1){
					gameOver.gotoAndStop(2);	
				}else
					gameOver.gotoAndStop(1);
			}
		}
		
		/**Here execute the event in the selection user.**/
		private function evGoSelection():void{
			if (gameOver != null){
				if (gameOver.currentFrame == 1){
					
					Main.audioBackground2.stop();
					evDestroyedGameOver();
					Main.instace.evResetLevel();
					Main.instace.evStartGame();
				}else{
					//here remove the progresse and points in the game (selection no)
					Main.instace.evResetDefultValue();
					Main.audioBackground2.stop()
					Main.instace.evStartGame();
				}
			}
		}
		
		/**Here we go to selecction events.**/
		protected function evNoGo(event:MouseEvent):void{evGoSelection();}
		
		/**Here we go to selecction events.**/
		protected function evYesGo(event:MouseEvent):void{evGoSelection();}
		
		/**Here we go to selecction feadback.**/
		protected function evYesOver(event:MouseEvent):void{
			if (gameOver != null)
				gameOver.gotoAndStop(1);
		}
		
		/**Here we go to selecction feadback.**/
		protected function evNoOver(event:MouseEvent):void{
			if (gameOver != null)gameOver.gotoAndStop(2);
		}
		
		/**Here deleted the Lost and events for this.**/
		public function evDestroyedGameOver():void{
			if (gameOver != null){
				gameOver.tiggerYes.removeEventListener(MouseEvent.MOUSE_OVER ,evYesOver);
				gameOver.tiggerNo.removeEventListener(MouseEvent.MOUSE_OVER ,evNoOver);
				
				gameOver.tiggerYes.removeEventListener(MouseEvent.MOUSE_DOWN ,evYesGo);
				gameOver.tiggerNo.removeEventListener(MouseEvent.MOUSE_DOWN ,evNoGo);
				
				Main.mainStage.removeEventListener(KeyboardEvent.KEY_DOWN ,selectionKey);
				Main.mainStage.removeChild(gameOver);
				
				gameOver = null;
			}
		}
		
		/**Here deleted the win and events for this.**/
		public function evDestroyedWin():void{
			if (win != null){
				win.removeEventListener(MouseEvent.CLICK, evMouseNextLevel);
				Main.mainStage.removeEventListener(KeyboardEvent.KEY_DOWN, evKeyNextLevel);
				
				Main.mainStage.removeChild(win);
				win = null;
			}
		}
	}
}