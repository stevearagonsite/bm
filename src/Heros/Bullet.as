package Heros{
	public class Bullet{
		public var damage:int = 1;//here have the power of my bullet
		public var speedY:int = 12;//this is general speed of my bullet
		public var speedX:int = 0;
		public var model:MCbullet2; //this is my movieclip
		
		public var isDestroyed:Boolean;//this boolean can deleted my bullet
		
		
		public function Spawn(posX:int, posY:int, damage:int):void{
			this.damage = damage;
			isDestroyed = false;//first parameter of my bullet
			
			model = new MCbullet2;
			Main.layer1.addChild(model);//painting the hero in the stage
			model.x = posX;//position in the stage wiht the hero
			model.y = posY;
			model.trigger.visible = false;
		}
		
		/**Here conect with update general.**/
		public function evUpdate():void{
			if (model != null){
				model.y -= speedY;//Move of my bullet.
				model.x -= speedX;
				if (model.y <= 0 )
					evDestroyed();	
			}
		}
		
		/**Execute this event when my bullet is out stage or hit whint asteroid.**/
		public function evDestroyed():void{
			if (model != null && !isDestroyed){
				Main.layer1.removeChild(model);
				model = null;
				isDestroyed = true
			}
		}
		
		/**Execute this event when my bullet is out stage or hit whint asteroid.**/
		public function evDestroyedOut():void{
			if (model != null){
				Main.layer1.removeChild(model);
				model = null;
				isDestroyed = true
			}
		}
	}
}