package Heros{
	
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip;
	import flash.events.KeyboardEvent;

	public class Hero{
		
		public var model:MovieClip;
		public const speed:uint = 5;//this is my speed hero in the stage
		public var aceleration:Number = 1;
		public var life:int = 100;//this is the life of my hero --loading....
		public var damage:int = 1;
		public var activeSuperShoot:Boolean;
		public var timeToNormalShoot:int = 4000;
		public var currentTimeToNormalShoot:Number = timeToNormalShoot;
		
		public var isPressing_Left:Boolean;//this boolean is the direction for my hero in X
		public var isPressing_Right:Boolean;//this boolean is the direction for my hero in X
		public var isPressing_Down:Boolean;//this boolean is the direction for my hero in Y
		public var isPressing_Up:Boolean;//this boolean is the direction for my hero in Y
		public var isPressing_Shoot:Boolean;//this boolean dispatch a event the shoot
		
		public var isDestroyed:Boolean;//it is check by for in the Main
		
		public var canShoot:Boolean//loading..
		public var directionXOn:Boolean//loading...
		public var directionYOn:Boolean//loading....
		
		public var Shoot:int;//this is control for my hero (code ascii)
		public var Left:int;//this is control for my hero (code ascii)
		public var Right:int;//this is control for my hero (code ascii)
		public var Up:int;//this is control for my hero (code ascii)
		public var Down:int;//this is control for my hero (code ascii)
		
		/**Here create the hero in the stage and events of Keyboard, i can set where spawn my hero**/
		public function Spawn(posX:int, posY:int, modelType:int, Shoot:int, Left:int, Right:int, Up:int, Down:int, model:MovieClip):void{
			this.Shoot = Shoot;
			this.Left = Left;
			this.Right = Right;
			this.Up = Up;
			this.Down = Down;
			
			this.model = model;
			
			isDestroyed = false;
			
			Main.layer2.addChild(model);
			model.trigger.visible = false;
			model.gotoAndStop(1);
			
			model.x = posX;//this is initials position of my hero
			model.y = posY;
			
			//this is my events keyboard and link with update in the main for dispacth events in the screen
			Main.mainStage.addEventListener(KeyboardEvent.KEY_DOWN, evKeyDown);
			//this is my events keyboard and link with update in the main for dispacth events in the screen
			Main.mainStage.addEventListener(KeyboardEvent.KEY_UP, evKeyUp);
		}
		
		/**This is my events keyboard and link with update in the main for dispacth events in the screen**/
		protected function evKeyUp(event:KeyboardEvent):void{
			
			if (!isDestroyed && model != null){
				model.gotoAndStop(1);
				aceleration = 0;
			}
			//move and shoot when keyboard button is no presisng
			switch (event.keyCode){
				case Left:
					if (!isDestroyed){
						TweenMax.to(model, 0.25, {blurFilter:{blurX:0, blurY:0}});//here disable filter for the move
						isPressing_Left = false;//this boolean is the direction for my hero in X,it is off
						model.rotationZ = 0;
						directionXOn = false;
						break;
					}
				case Right:
					if (!isDestroyed){
						TweenMax.to(model, 0.25, {blurFilter:{blurX:0, blurY:0}});//here disable filter for the move
						isPressing_Right = false;//this boolean is the direction for my hero in X,it is off
						model.rotationZ = 0;
						directionXOn = false;
					}
					break;
				case Up:
					if (!isDestroyed){
						TweenMax.to(model, 0.25, {blurFilter:{blurX:0, blurY:0}});//here disable filter for the move
						isPressing_Up = false;//this boolean is the direction for my hero in X,it is off
						directionYOn = false;
						break;
					}
					
				case Down:
					if (!isDestroyed){
						TweenMax.to(model, 0.25, {blurFilter:{blurX:0, blurY:0}});//here disable filter for the move
						isPressing_Down = false;//this boolean is the direction for my hero in X,it is off
						directionYOn = false;
						break;
					}
					
				case Shoot:
					if (!isDestroyed){
						isPressing_Shoot = false;//this boolean dispatch a event the shoot, it is off
						canShoot = true;
						break;
					}
			}
			
		}
		
		/**This is my events keyboard and link with update in the main for dispacth events in the screen.**/
		protected function evKeyDown(event:KeyboardEvent):void{
			//move and shoot when keyboard button is pressing
			switch (event.keyCode){
				case Left:
					if (!isDestroyed){
						model.gotoAndStop(3);
						TweenMax.to(model, 1, {blurFilter:{blurX:7}});//here execute filter for the move
						isPressing_Left = true;//this boolean is the direction for my hero in X,it is on
						model.rotationZ = 350;
						directionXOn = true;
						break;
					}
				case Right:
					if (!isDestroyed){
						model.gotoAndStop(2);
						TweenMax.to(model, 1, {blurFilter:{blurX:7}});//here execute filter for the move
						isPressing_Right = true;//this boolean is the direction for my hero in X,it is on
						model.rotationZ = 10;
						directionXOn = true;
						break;
					}
				case Up:
					if (!isDestroyed){
						TweenMax.to(model, 1, {blurFilter:{blurY:7}});//here execute filter for the move
						isPressing_Up = true;//this boolean is the direction for my hero in Y,it is on
						directionYOn = true;
						break;
					}
				case Down:
					if (!isDestroyed){
						TweenMax.to(model, 1, {blurFilter:{blurY:7}});//here execute filter for the move
						isPressing_Down = true;//this boolean is the direction for my hero in Y,it is on
						directionYOn = true;
						break;
					}
					
				case Shoot:
					if (canShoot && !isDestroyed){
						isPressing_Shoot = true;//this boolean dispatch a event the shoot, it is on
					}
					break;
			}
		}
		
		/**Conect this function with Update of the Main.**/
		public function evUpdate():void{
			
			//here execute my boolean event and another condition of move only in the stage
			if (isPressing_Left && model.x > 0 && !isDestroyed){moveX(-1);
			}else if (isPressing_Right && model.x < Main.mainStage.stageWidth && !isDestroyed)
				moveX(+1);
			
			if (isPressing_Down && model.y < Main.mainStage.stageHeight && !isDestroyed){moveY(+1);
			}else if (isPressing_Up && model.y > Main.mainStage.stageHeight/3 && !isDestroyed)
				moveY(-1);
			
			EvTimeToNormalShoot();
			
			//here execute my boolean event
			if (isPressing_Shoot && !isDestroyed){
				activeSuperShoot ? Main.instace.EvSpecialShoot( model.x, model.y, damage, 3): 
					Main.instace.evShoot(model.x, model.y, damage);
				isPressing_Shoot = false;
				canShoot = false;
			}
		}
		
		/****/
		private function EvTimeToNormalShoot():void{
			if (activeSuperShoot){
				currentTimeToNormalShoot -= 1000/Main.mainStage.frameRate;
				if (currentTimeToNormalShoot < 0){
					currentTimeToNormalShoot = timeToNormalShoot;
					activeSuperShoot = false;
				}
			}
		}
		
		/**Execute pause when i need.**/
		public function evPause():void{
			Main.mainStage.removeEventListener(KeyboardEvent.KEY_DOWN, evKeyDown);
			Main.mainStage.removeEventListener(KeyboardEvent.KEY_UP, evKeyUp);
		}
		/**This is the resume.**/
		public function evRemovePause():void{
			Main.mainStage.addEventListener(KeyboardEvent.KEY_DOWN, evKeyDown);
			Main.mainStage.addEventListener(KeyboardEvent.KEY_UP, evKeyUp);
		}
		
		/**This events is moves my hero in the stage Y**/
		public function moveY(direction:int):void{
			if (aceleration < 1 || aceleration > -1 && !directionXOn){
				if (direction > 0){ aceleration += 0.1;
				}else if (direction <0) aceleration -= 0.1;
			}
			model.y += aceleration + speed * direction;
		}
		
		/**This events is moves my hero in the stage X**/
		public function moveX(direction:int):void{
			if (aceleration < 1 || aceleration > -1 && !directionYOn){
				if (direction > 0){ aceleration += 0.1;
				}else if (direction <0) aceleration -= 0.1;
			}
			model.x += aceleration + speed * direction;
		}
		
		/**When hero was attack.**/
		public function evRemoveLife(damage:int,type:int):void{
			life -= damage;
			if (type == 1){
				Main.life1 -= damage;
			}else
				Main.life2 -= damage;
		}
		
		/**When hero was collission with power life.**/
		public function evAddLife(add:int,type:int):void{
			life += add;
			if (type == 1){
				Main.life1 += add;
			}else
				Main.life2 += add;
		}
		
		/**Destroy the assets in the stage.**/
		public function evDestroyed():void{
			
			if (model != null){
				isDestroyed = true
				Main.layer2.removeChild(model);
				model = null;
			}	
		}
	}
}