package Heros{
	
	import com.greensock.TweenMax;
	
	import flash.events.KeyboardEvent;

	public class SuperHero{
		
		public var isPressing_Left:Boolean;//this boolean is the direction for my hero in X
		public var isPressing_Right:Boolean;//this boolean is the direction for my hero in X
		public var isPressing_Down:Boolean;//this boolean is the direction for my hero in Y
		public var isPressing_Up:Boolean;//this boolean is the direction for my hero in Y
		public var isPressing_Shoot:Boolean;//this boolean dispatch a event the shoot
		public var isDestroyed:Boolean;//it is check by for in the Main
		public var canShoot:Boolean//loading..

		public var Shoot:int;//this is control for my hero (code ascii)
		public var Left:int;//this is control for my hero (code ascii)
		public var Right:int;//this is control for my hero (code ascii)
		public var Up:int;//this is control for my hero (code ascii)
		public var Down:int;//this is control for my hero (code ascii)
		
		public var model:MCheroSuper;
		public const speed:uint = 7;//this is my speed hero in the stage
		public var life:int = 100;//this is the life of my hero --loading....
		public var damage:int = 3;
		public var activeSuperShoot:Boolean = false;
		public var timeToNormalShoot:int = 8000;
		public var currentTimeToNormalShoot:Number = timeToNormalShoot;
		
		/**When spawn the super hero i need parameters for this.**/
		public function spawn(posX:int, posY:int, modelType:int, Shoot:int, Left:int, Right:int, Up:int, Down:int):void{
			this.Shoot = Shoot;
			this.Left = Left;
			this.Right = Right;
			this.Up = Up;
			this.Down = Down;
			
			isDestroyed = false;
			
			model = new MCheroSuper;
			Main.layer2.addChild(model);
			
			model.x = posX;//this is initials position of my super hero
			model.y = posY;
			
			model.trigger.visible = false;
			model.gotoAndStop(1);
			
			//this is my events keyboard and link with update in the main for dispacth events in the screen
			Main.mainStage.addEventListener(KeyboardEvent.KEY_DOWN, evKeyDown);
			//this is my events keyboard and link with update in the main for dispacth events in the screen
			Main.mainStage.addEventListener(KeyboardEvent.KEY_UP, evKeyUp);	
		}
		
		/**This is my events keyboard and link with update in the main for dispacth events in the screen.**/
		protected function evKeyUp(event:KeyboardEvent):void{
			
			if (!isDestroyed && model != null)model.gotoAndStop(1);
			//move and shoot when keyboard button is no presisng
			switch (event.keyCode){
				case Left:
					if (!isDestroyed){
						TweenMax.to(model, 0.25, {blurFilter:{blurX:0, blurY:0}});//here disable filter for the move
						isPressing_Left = false;//this boolean is the direction for my hero in X,it is off
						break;
					}
					
				case Right:
					if (!isDestroyed){
						TweenMax.to(model, 0.25, {blurFilter:{blurX:0, blurY:0}});//here disable filter for the move
						isPressing_Right = false;//this boolean is the direction for my hero in X,it is off
					}
					break;
				
				case Up:
					if (!isDestroyed){
						TweenMax.to(model, 0.25, {blurFilter:{blurX:0, blurY:0}});//here disable filter for the move
						isPressing_Up = false;//this boolean is the direction for my hero in X,it is off
						break;
					}
					
				case Down:
					if (!isDestroyed){
						TweenMax.to(model, 0.25, {blurFilter:{blurX:0, blurY:0}});//here disable filter for the move
						isPressing_Down = false;//this boolean is the direction for my hero in X,it is off
						break;
					}
					
				case Shoot:
					if (!isDestroyed){
						isPressing_Shoot = false;//this boolean dispatch a event the shoot, it is off
						canShoot = true;
						break;
					}
			}
		}
		
		/**Move and shoot when keyboard button is presing.**/
		protected function evKeyDown(event:KeyboardEvent):void{
			
			switch (event.keyCode){
				case Left:
					if (!isDestroyed){
						model.gotoAndStop(3);
						TweenMax.to(model, 1, {blurFilter:{blurX:7}});//here execute filter for the move
						isPressing_Left = true;//this boolean is the direction for my hero in X,it is on
						break;
					}
					
				case Right:
					if (!isDestroyed){
						model.gotoAndStop(2);
						TweenMax.to(model, 1, {blurFilter:{blurX:7}});//here execute filter for the move
						isPressing_Right = true;//this boolean is the direction for my hero in X,it is on
						break;
					}
					
				case Up:
					if (!isDestroyed){
						TweenMax.to(model, 1, {blurFilter:{blurY:7}});//here execute filter for the move
						isPressing_Up = true;//this boolean is the direction for my hero in Y,it is on
						break;
					}
					
				case Down:
					if (!isDestroyed){
						TweenMax.to(model, 1, {blurFilter:{blurY:7}});//here execute filter for the move
						isPressing_Down = true;//this boolean is the direction for my hero in Y,it is on
						break;
					}
					
				case Shoot:
					if (canShoot && !isDestroyed){
						isPressing_Shoot = true;//this boolean dispatch a event the shoot, it is on
					}
					break;
			}
		}
		
		/**it is in the Main evUpdate and this is events when user is pressing keyboard (direction in my screen)**/
		public function evUpdate():void{
			
			if (isPressing_Left && model.x > 0 && !isDestroyed){
				//here execute my boolean event and another condition of move only in the stage.
				moveX(-1);
			}else if (isPressing_Right && model.x < Main.mainStage.stageWidth && !isDestroyed){
				//here execute my boolean event and another condition of move only in the stage.
				moveX(+1);
			}
			
			if (isPressing_Down && model.y < Main.mainStage.stageHeight && !isDestroyed){
				//here execute my boolean event and another condition of move only in the stage
				moveY(+1);
			}else if (isPressing_Up && model.y > Main.mainStage.stageHeight/3 && !isDestroyed){
				//here execute my boolean event and another condition of move only in the stage
				moveY(-1);
			}
			
			EvTimeToNormalShoot();
			
			if (isPressing_Shoot && !isDestroyed){//When this is active, create new shoot.
				activeSuperShoot ? Main.instace.EvSpecialShoot( model.x, model.y, damage, 5): 
					Main.instace.evShoot(model.x, model.y, damage);
				isPressing_Shoot = false;
				canShoot = false;
			}
		}
		
		/**Disable the special shoot**/
		private function EvTimeToNormalShoot():void{
			if (activeSuperShoot){
				currentTimeToNormalShoot -= 1000/Main.mainStage.frameRate;
				if (currentTimeToNormalShoot < 0){
					currentTimeToNormalShoot = timeToNormalShoot;
					activeSuperShoot = false;
				}
			}
		}
		
		/**When it is pressing key pause execute this event.**/
		public function evPause():void{
			Main.mainStage.removeEventListener(KeyboardEvent.KEY_DOWN, evKeyDown);
			Main.mainStage.removeEventListener(KeyboardEvent.KEY_UP, evKeyUp);
		}
		
		/**When it is pause execute this event remove pause.**/
		public function evRemovePause():void{
			Main.mainStage.addEventListener(KeyboardEvent.KEY_DOWN, evKeyDown);
			Main.mainStage.addEventListener(KeyboardEvent.KEY_UP, evKeyUp);
		}
		
		/**This events is moves my hero in the stage Y.**/
		public function moveY(direction:int):void{ model.y += speed * direction; }
		
		/**This events is moves my hero in the stage X.**/
		public function moveX(direction:int):void{ model.x += speed * direction; }
		
		/**Get damage for this hero.**/
		public function evRemoveLife(damage:int):void{ Main.superLife -= damage; }
		
		/**When hero was collission with power life.**/
		public function evAddLife(add:int):void{ Main.superLife += add; }
		
		/**This event active the super hero.**/
		public function evMixer(posX:int, posY:int):void{
			
			model.x = posX;
			model.y = posY;
			
			//this is my events keyboard and link with update in the main for dispacth events in the screen is pause
			Main.mainStage.addEventListener(KeyboardEvent.KEY_DOWN, evKeyDown);
			//this is my events keyboard and link with update in the main for dispacth events in the screen is pause
			Main.mainStage.addEventListener(KeyboardEvent.KEY_UP, evKeyUp);
			
			//here is the check mixer
			Main.isMixerHeros = true;
		}
		
		/**This event disable the super hero in other position.**/
		public function evDisableMixer():void{
			model.x = 2000;
			model.y = 0;//-----this position take hero pause
			
			//this is my events keyboard and link with update in the main for dispacth events in the screen is pause
			Main.mainStage.removeEventListener(KeyboardEvent.KEY_DOWN, evKeyDown);
			//this is my events keyboard and link with update in the main for dispacth events in the screen is pause
			Main.mainStage.removeEventListener(KeyboardEvent.KEY_UP, evKeyUp);
			
			//here is the check mixer
			Main.isMixerHeros = false;
		}
		
		/**Here destroy the model and other parameters.**/
		public function evDestroyed():void{
			if (model != null){
				isDestroyed = true
				Main.layer2.removeChild(model);
				model = null;
			}	
		}
	}
}