package
{	
	public class Machine
	{
		public var model:MCmachine;
		public var isDestroyed:Boolean = false;//this check to be
		public var speed:int = 1;// speed accommodate in the stage
		public var posY:int;//point of the Y in the stage, it will accommodate
		public const rotationValue:int = 1;
		
		public function Machine()
		{
		}
		
		public function Spawn():void//here the machine spawn in the stage
		{
			model = new MCmachine;
			Main.layer1.addChild(model);
			model.trigger.visible = false;
			
			model.x = Main.instace.getRandom(model.width, Main.mainStage.stageWidth- model.width);
			
			posY = Main.instace.getRandom
				(Main.mainStage.stageHeight/3, Main.mainStage.stageHeight-model.height*2);
		}
		
		
		public function evUpdate():void//this conect with update general that is in the Main
		{
			if (model != null)
			{
				model.mc_rotation.rotation += rotationValue;
				
				if (model.y <= posY )
				{
					model.y += speed;
				}
			}
			
		}
		
		public function evDestroyed():void//when my machine collision with hero is destroyed is this event
		{
			if (model != null)
			{
				isDestroyed = true;
				Main.layer1.removeChild(model);
				model = null;
			}
		}
	}
}