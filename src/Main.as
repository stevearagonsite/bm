package{
	
	import com.greensock.TweenMax;
	import com.greensock.plugins.MotionBlurPlugin;
	import com.greensock.plugins.TweenPlugin;
	
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	
	import Ambient.Background;
	import Ambient.Camera2D;
	import Ambient.SoundController;
	
	import GUI.HUD;
	import GUI.Menu;
	import GUI.Pause;
	import GUI.WinLost;
	
	import Heros.Bullet;
	import Heros.Hero;
	import Heros.SuperHero;
	
	import PowersPack.PowersManager;
	
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="0x000000")]//Is the screen size and others opcions.
	public class Main extends Sprite{
		
		public static var mainStage:Stage;//Here i can to use the stage in other class.
		public static var instace:Main;//Here i can to use for run function.
		public static var audio:SoundController;//Here is events SFX and Sountrack.
		
		public static var cam:Camera2D;//Here is the view and events FX.
		public var menu:Menu;//Here have control main menu.
		public var pause:Pause;//Here is the menu pause and the events execute other class.
		public var winLost:WinLost;//Here has the screens the game over and win.

		public static var level:Sprite = new Sprite;//Here save my assets games for the camera.
		public static var layer1:Sprite = new Sprite;//Here is save background FX and the asteroids.
		public static var layer2:Sprite = new Sprite;//Here is the asteroids and the heros.
		public static var layer3:Sprite = new Sprite;//Here is FX and Heros.
		
		public static var myEnemies:Array = new Array();//Here save my asteroids.
		public static var myHeros:Array = new Array();//Here save my heros.
		public static var myBullets:Array = new Array();//Here save my bullets.
		public static var myMachines:Array = new Array();//Here save my bullets.
		public static var myPowers:Array = new Array();//Here save my powers.
		
		public var timeToDeleted:int = 2;//It is time to Spawn deleted the general time.
		public var timeToSpawnEnemy:int = 3000;// It is time to Spawn enemys general.
		public var currentTimeToSpawnEnemy:Number = timeToSpawnEnemy;//It is time to Spawn enemys.
		public var timeToSpawnEnemy2:int = 1000;// It is time to Spawn enemys general.
		public var currentTimeToSpawnEnemy2:Number = timeToSpawnEnemy2;//It is time to Spawn enemys. 
		public var timeToSpawnEnemy3:int = 4000;// It is time to Spawn enemys general.
		public var currentTimeToSpawnEnemy3:Number = timeToSpawnEnemy3;//It is time to Spawn enemys.
		public var timeToSpawnMachine:int = 5000;//I have time to spawn goals.
		public var currentTimetoSpawnMachine:Number = timeToSpawnMachine;//It is time to spawn machines.
		public var timeToSpawnPowers:int = 8000;//It is time for powers general spawn.
		public var currentTimeToSpawnPowers:Number = timeToSpawnPowers;//It is time to spawn powers.
		
		public static var totalPoints:int = 0;//Points in the mission.
		public static var numMachinesSpawn:int = 0;//Points to spawn in the level.
		public static var goal:int = 5;//How many machines i should to have?
		public static var currentLevel:int = 1;//Update the level for the users.
		
		public static var isPause:Boolean = false;//Here check situation.
		public var isFullScreen:Boolean = true;//Here check situation.
		public var isLocked:Boolean = false;//For somethings movieclips.
		public static var isBlockSpace:Boolean = false;//This help of the in the zoom bug.
		
		public static var background:Background;//Here have background FX.
		public static var hud:HUD;//I am in the mainStage.
		public static var isIce:Boolean;//Here check the power is active or no.
		public var enemy:Enemy;//Here have events the enemy1,enemy2,enemy3.
		public var machine:Machine;//Here have events the goal or machines.
		public var powersManager:PowersManager;//Here have events the all powers.
		public var attack1:Bullet;//This is the main bullet.
		
		public var hero1:MChero1;//This is the moviclip the hero 1 and this hero save in the class Hero(heroA).
		public var hero2:MChero2;//This is the moviclip the hero 1 and this hero save in the class Hero(heroB).
		public static var superHero:SuperHero;//Super hero have the movieclip and events for this.
		public static var isMixerHeros:Boolean = false;//Here check mixer.
		public static var life1:int = 100;//This var have life for hero 1.
		public static var life2:int = 100;//This var have life for hero 2.
		public static var superLife:int = 100;//This var have life for super hero.
		public var heroA:Hero;//This is the class hero and it save hero1 for myHeros.
		public var heroB:Hero;//This is the class hero and it save hero2 for myHeros.
		
		public static var audioBackground:SoundController = new SoundController( new SNDsoundtrack());
		//In the menu is the ambient soundtrack.
		public static var audioBackground2:SoundController = new SoundController( new SNDsoundtrak2());
		//In the level is the ambient soundtrack.
		public static var audioDamage1:SoundController = new SoundController( new SNDdamage1());
		//This execute in the radom sound when hero was damage.
		public static var audioDamage2:SoundController = new SoundController( new SNDdamage2());
		//This execute in the radom sound when hero was damage.
		public static var audioMixerOut:SoundController = new SoundController( new SNDmixerOut());
		//Here execute when heros is out mixer.
		public static var audioMixer:SoundController = new SoundController(new SNDmixer());
		//Here execute when heros is mixer.
		public static var audioShoot:SoundController = new SoundController(new SNDshoot());
		//Here execute when execute the class bullet(main bullet).
		public static var audioGoal:SoundController = new SoundController(new SNDgoal);
		//This sound execute when hero is in the goal (machin collission).
		public static var audioDamageEnemy:SoundController = new SoundController(new SNDdamageEnemy());
		//This sound execute when bullet is in the enemy1,enemy2enemy3 (main bullet collission).
		public static var audioWin:SoundController = new SoundController( new SNDwin());
		//this is the sound for the win screen (execute the class SoundController)
		public static var audioGameOver:SoundController = new SoundController( new SNDgameOver());
		//this is the sound for the Game Over screen (execute the class SoundController)
		
		/**Here start the initials parameters the game.**/
		public function Main(){
			TweenPlugin.activate([MotionBlurPlugin]);//Crack tweenMax.
			
			mainStage = stage;//Here have my moviclips.
			instace = this;//This execute in other class.
			
			//This is screen parameters.
			mainStage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;//Here i can scale the elements.
			mainStage.scaleMode = StageScaleMode.SHOW_ALL;//Here scale the elements.
			
			level.addChild(layer1);//This element is child of level, is save background FX and the asteroids.
			level.addChild(layer2);//This element is child of level, is the heros.
			level.addChild(layer3);//This element is child of level, is FX and Heros.
			
			evMenu();
		}
		
		/**Here execute the menu class, this class execute evStartGame.**/
		public function evMenu():void{
			menu = new Menu;
			menu.spawn();//Painting the menu and execute the events for the user.
		}
		
		/**This is start the game and painting assets in the screen and execute events.**/
		public function evStartGame():void{
			
			background = new Background;//This class have wallpaper and FX velocitiy.
			background.spawn();//Create the movieclip in the mainStage.
			cam = new Camera2D();
			cam.on();//Active the camera.
			cam.addToView(level);//Camera is parent the level(objets zoom).
			hud = new HUD;//Create the HUD, for defult create the movieclips in the mainStage.
			powersManager = new PowersManager;//I can control of all powers;
			Main.audioBackground2.EvPlayLoop();//Soundtrack background.
			
			//Here create super hero.
			superHero = new SuperHero;
			//This is the position initial to stage
			superHero.spawn(mainStage.stageWidth/4, 
				mainStage.stageHeight/2+mainStage.stageHeight/4,2,
				Keyboard.E, Keyboard.A, Keyboard.D, Keyboard.W,Keyboard.S);
			superHero.evDisableMixer();
			//myHeros.push(superHero);
			
			//Here create the hero1 and hero2.
			evCreateHeros();
			//Here remove the mouse in the stage.
			Mouse.hide();
			
			//Here have the all updates the class and Main update.
			mainStage.addEventListener(KeyboardEvent.KEY_UP, evKeyGame);
			//Here have events of the keyboard in the stage.
			mainStage.addEventListener(Event.ENTER_FRAME, evUpdate);
		}
		
		
		/**Create the heros in the call mixer Out**/
		private function evCreateHeros():void{
			heroA = new Hero;//Create the hero.
			hero1 = new MChero1;
			//This is the position initial to stage, parameters initals for the hero.
			heroA.Spawn(mainStage.stageWidth/4, 
				mainStage.stageHeight/2+mainStage.stageHeight/4,2,
				Keyboard.E, Keyboard.A, Keyboard.D, Keyboard.W,Keyboard.S,hero1);
			
			heroB = new Hero;//Create the hero.
			hero2 = new MChero2;
			//This is the position initial to stage, parameters intials for the hero.
			heroB.Spawn(mainStage.stageWidth/4*3, 
				mainStage.stageHeight/2+mainStage.stageHeight/4,1,
				Keyboard.U, Keyboard.J, Keyboard.L, Keyboard.I, Keyboard.K, hero2);
			
			//now is in the array myHeros
			myHeros.push(heroA);
			myHeros.push(heroB);
		}
		
		/**All updates here.**/
		protected function evUpdate(event:Event):void{
			//Here update check which heros should to execute evUpdate.
			if (!isMixerHeros){
				evUpdateHeros();
			}else if (isMixerHeros)superHero.evUpdate();
			
			hud.evUpdate();
			//This is the class and execute events update for the screen. here put in the screen the progress.
			evMixer();
			//Here execute the events mixer and fx mixer.
			background.evUpdate();
			//This is the class and execute events update for the screen (Feadback background move).
			updateTimers();
			//This function have timers to spawn enemies, machines and powers.
			evUpdateEnemies();
			//This function have update the array enemies.
			evUpdateMachines();
			//This function have update the array machines.
			evUpdateBullet();
			//This function have update the array bullets.
			evUpdatePowers();
			//this function have update the array powers.
			
			checkCollision();//This function check collision the all objects in the screen.
			checkProgress();//This function save the progress for the user.
		}
		
		/**Check the progress and create conclusion for the level.**/
		private function checkProgress():void{
		
			if (totalPoints >= goal && hud != null){
				//To be HUD?, events for win level.
				hud.timeActive = true;//This boolean active a timer win.
				//Check timer win is true in the class.
				if (hud.timeWin){
					evRemoveAll();
					winLost = new WinLost;
					Mouse.show();
					winLost.evWin();//Here create the class and events for win screen.
				}else if (life1 <= 0 || life2 <= 0 || superLife <= 0){
					//This event execute Game Over.
					evRemoveAll();
					Mouse.show();
					winLost = new WinLost;
					winLost.evGameOver();
				}
			}else if (life1 <= 0 || life2 <= 0 || superLife <= 0){
				//This event execute Game Over.
				evRemoveAll();
				Mouse.show();
				winLost = new WinLost;
				winLost.evGameOver();
			}
		}
		
		/**Check collision with assets game.**/
		public function checkCollision():void{
		
			/**Check collision bullets with enemies**/
			for (var i_bullets:int = myBullets.length-1; i_bullets >= 0; i_bullets--){//Check bullets.
				for (var i_enemies:int = myEnemies.length-1; i_enemies >= 0 ; i_enemies--){//Check enemies.
					if (!myBullets[i_bullets].isDestroyed && !myEnemies[i_enemies].isDestroyed &&
						myBullets[i_bullets].model.trigger.hitTestObject(myEnemies[i_enemies].model.trigger)){
						//To be bullets and enemies??, check collision.
						evExplotion(myBullets[i_bullets].model.x ,myBullets[i_bullets].model.y,0.1);//Feedback explotion crash.
						myBullets[i_bullets].evDestroyed();//Destroid bullet.
						myEnemies[i_enemies].evRemoverLife();//Remove life asteroids and feedback destroy.
					}
				}
			}
			
			/**Check collision powers with enemies**/
			for (var i_powers:int = myPowers.length-1; i_powers >= 0; i_powers--){//Check powers.
				if (!isMixerHeros){//Here check collision when the user have two heros.
					for (var i_heros2:int = myHeros.length-1; i_heros2 >= 0 ; i_heros2--){//Check heros.
						if (!myPowers[i_powers].isDestroyed && !myHeros[i_heros2].isDestroyed &&
							myPowers[i_powers].model.trigger.hitTestObject(myHeros[i_heros2].model.trigger)){
							FXmixer(myPowers[i_powers].model.x ,myPowers[i_powers].model.y);
							myPowers[i_powers].ActivePower(i_heros2);
							myPowers[i_powers].evDestroyedOut();
						}
					}
				}else{//Here check collision when the user have the super hero.
					if (!myPowers[i_powers].isDestroyed && !superHero.isDestroyed &&
						myPowers[i_powers].model.trigger.hitTestObject(superHero.model.trigger)){
						FXmixer(myPowers[i_powers].model.x ,myPowers[i_powers].model.y);
						myPowers[i_powers].ActivePower(2);
						myPowers[i_powers].evDestroyedOut();
					}
				}
			}
			
			/**Check collision machines with heros**/
			for (var i_machines:int = myMachines.length-1; i_machines >=0; i_machines--){//Check machines.
				if (!isMixerHeros){//Here check collision when the user have two heros.
					for (var i_heros:int = myHeros.length-1; i_heros >=0 ; i_heros--){//Check heros.
						if (!myMachines[i_machines].isDestroyed && !myHeros[i_heros].isDestroyed &&
							myMachines[i_machines].model.trigger.hitTestObject(myHeros[i_heros].model.trigger)){
							//To be machines and enemies??, check collision.
							evGoal(myMachines[i_machines].model.x, myMachines[i_machines].model.y);//Here FX Goal, call a class.
							myMachines[i_machines].evDestroyed();
							totalPoints += 1;
						}
					}	
				}else{//Here check collision when the user have the super hero.
					if (!myMachines[i_machines].isDestroyed && !superHero.isDestroyed &&
						myMachines[i_machines].model.trigger.hitTestObject(superHero.model.trigger)){
						//To be machines??
						evGoal(myMachines[i_machines].model.x, myMachines[i_machines].model.y);//Here FX Goal, call the class.
						myMachines[i_machines].evDestroyed();
						totalPoints += 1;
					}
				}
			}	
		
			/**Check collision enemies with heros.**/
			for (var i_enemies2:int = myEnemies.length-1; i_enemies2 >= 0; i_enemies2--){ 
				if (!isMixerHeros){//Here check collision for two heros.
					if (!myEnemies[i_enemies2].isDestroyed && !myHeros[0].isDestroyed &&
						myEnemies[i_enemies2].model.trigger.hitTestObject(myHeros[0].model.trigger)){
						//To be heros and enemies??, check collision.
						myHeros[0].evRemoveLife(myEnemies[i_enemies2].damage,1);
						if(!isLocked)evDamage(myHeros[0].model.x ,myHeros[0].model.y);//here FX damage.
					}else if (!myEnemies[i_enemies2].isDestroyed && !myHeros[1].isDestroyed &&
					myEnemies[i_enemies2].model.trigger.hitTestObject(myHeros[1].model.trigger)){
						//To be heros and enemies??, check collision.
						myHeros[1].evRemoveLife(myEnemies[i_enemies2].damage,0);
						if(!isLocked)evDamage(myHeros[1].model.x ,myHeros[1].model.y);//Here FX damage.
					}
				}else{//Here check collision when the user have super hero.
					if (!myEnemies[i_enemies2].isDestroyed && !superHero.isDestroyed &&
						myEnemies[i_enemies2].model.trigger.hitTestObject(superHero.model.trigger)){
						//To be heros and enemies??, check collision.
						superHero.evRemoveLife(myEnemies[i_enemies2].damage);
						if(!isLocked)evDamage(superHero.model.x ,superHero.model.y);//Here FX damage.
					}
				}
			}
		}
		
		/**This function have events the keyboard play.**/
		protected function evKeyGame(event:KeyboardEvent):void{
		
			switch(event.keyCode){
				case Keyboard.SPACE:{//This key should be active when the super hero is in the screen.
					if (isMixerHeros && !isBlockSpace)evMixerOut();	//Block in the FX mixer.
					break;
				}
				case Keyboard.SHIFT:{//This key should active the HUD or no.
					if (pause == null)hud.evVisible();
					break;
				}
				case Keyboard.F2:{//Here create button pause for my game.
					evPause();
					break;
				}
				case Keyboard.F4:{//Here create button pause for my game.
					mainStage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;//here i can scale the elements
					mainStage.scaleMode = StageScaleMode.SHOW_ALL;//here scale the elements
					break;
				}
				case Keyboard.ESCAPE:{//Here create button pause for my game.
					mainStage.nativeWindow.x = mainStage.nativeWindow.y = 0;
					break;
				}
			}
		}
		
		/**here execute menu pause**/
		private function evPause():void{
		
			if (isPause){//Boolean check the state of pause.

				isPause = false;
				Mouse.show();//Active mouse.
				
				//Create the menu pause.
				pause = new Pause;
				pause.Spawn();//Menu in the mainStage.
				mainStage.removeEventListener(Event.ENTER_FRAME,evUpdate);//Remove events for the game.
				audioBackground2.volume = 0.1;//Create ambient pause.
					
				//Execute pause in the class, hero by hero.
				if (superHero != null)superHero.evPause();
				for (var i_heros:int = myHeros.length-1; i_heros >= 0; i_heros--)myHeros[i_heros].evPause();
				
				//Execute pause in the class, enemy by enemy. 
				for (var i_enemies:int = myEnemies.length-1; i_enemies >= 0; i_enemies--)myEnemies[i_enemies].evPause();
				
			}else if (!isPause)//Disable menu pause (control boolean pause).
				removePause();//Execute funtion for deleted menu pause, active resume.
		}
		
		/**Here remove the menu pause and active resume.**/
		public function removePause():void{
			
			isPause = true;//Boolean for pause.
			Mouse.hide();//Disable mouse.
			mainStage.addEventListener(Event.ENTER_FRAME, evUpdate);//Active resume.
			
			//Here check for active resume.
			if (pause != null){
				audioBackground2.volume = 1
				pause.evRemove();
				pause = null;
			}
			for (var i_heros:int = myHeros.length-1; i_heros >= 0; i_heros--) //Hero by hero.
				myHeros[i_heros].evRemovePause();//Resume for the class.
			for (var i_enemies:int = myEnemies.length-1; i_enemies >= 0; i_enemies--) //Enemy by enemy.
				myEnemies[i_enemies].evRemovePause();//Resume for the class.
			if (superHero != null)
				superHero.evRemovePause();//Resume for the class.
		}
		
		/**Execute when is press down space and super hero is active.**/
		private function evMixerOut():void{
		
			FXmixerOut(superHero.model.x,superHero.model.y);//Feadback FX mixerout.
			audioMixerOut.play();//SFX mixer out.
			
			superHero.evMixer(2000,0);//Disable super hero out in the screen.
			evCreateHeros();//Here active hero1 and hero2(create new heros).
			isMixerHeros = false;//Disable boolean.
		}
		
		/**Here have events mixer hero.**/
		private function evMixer():void{
			
			//To be the two heros and is Mixer ?
			if (myHeros != null && myHeros.length == 2 
				&& !myHeros[0].isDestroyed && !myHeros[1].isDestroyed &&
				myHeros[0].model.trigger.hitTestObject(myHeros[1].model.trigger) && !isMixerHeros){
				
				superHero.evMixer(myHeros[1].model.x,myHeros[1].model.y);//Here put the super hero in the screen.
				audioMixer.play();//Play the sound mixer.
				audioMixer.x = myHeros[1].model.x;//Pan the sound mixer.
				myHeros[0].evDestroyed();//Deleted the hero1.
				myHeros[1].evDestroyed();//Deleted the hero2.
				FXmixer(superHero.model.x ,superHero.model.y);//Position mixer heros.
				
				isBlockSpace = true;//Block the key space for the fx (zoom camera).
				evZoomFX(superHero.model.x ,superHero.model.y,1.5);//FX zoom camera.
			}
		}
		
		/**Create the FX smooth zoom and delay.**/
		private function evZoomFX(posX:int,posY:int,zoom:Number):void{
			cam.lookAt(superHero.model.trigger);//Here chage the hero.
			mainStage.frameRate = 30;//Delay in the stage.
			cam.smoothZoom = zoom;
		}		
		
		/**Here i can control to my spawn asteroids and spawn machines.**/
		private function updateTimers():void{
			var timeRate:Number = 1000 / stage.frameRate;
			
			currentTimeToSpawnEnemy -= timeRate;//Here i can control to my spawn asteriods.
			if(currentTimeToSpawnEnemy <= 0){
				spawnEnemy();
				if (timeToSpawnEnemy >= 200)
					timeToSpawnEnemy -= timeToDeleted;
				currentTimeToSpawnEnemy = timeToSpawnEnemy;
			}
			
			currentTimeToSpawnEnemy2 -= timeRate;//Here i can control to my spawn asteriods2.
			if(currentTimeToSpawnEnemy2 <= 0){
				spawnEnemy2();
				if (timeToSpawnEnemy2 >= 200)
					timeToSpawnEnemy2 -= timeToDeleted-1;
				currentTimeToSpawnEnemy2 = timeToSpawnEnemy2;
			}
			
			if (currentLevel >= 3){
				currentTimeToSpawnEnemy3 -= timeRate;//Here i can control to my spawn asteriods3.
				if(currentTimeToSpawnEnemy3 <= 0){
					spawnEnemy3();
					if (timeToSpawnEnemy3 >= 200)
						timeToSpawnEnemy3 -= timeToDeleted-1;
					currentTimeToSpawnEnemy3 = timeToSpawnEnemy3;
				}
			}
			
			if (numMachinesSpawn < goal){//Here i can control to my spawn Machines.
				currentTimetoSpawnMachine -= timeRate;
				if (currentTimetoSpawnMachine <= 0 && goal >= myMachines.length+1){
					spawnMachine();
					currentTimetoSpawnMachine = timeToSpawnMachine;
				}
			}
			
			currentTimeToSpawnPowers -= timeRate;//Here i can control to my spawn powers;
			if (currentTimeToSpawnPowers <= 0){
				spawnPowers();
				timeToSpawnPowers += 100;
				currentTimeToSpawnPowers = timeToSpawnPowers;
			}
		}
		
		/**Here execute update of my class Heros.**/
		private function evUpdateHeros():void{
			for(var i:int = myHeros.length-1; i >= 0; i--){
				if(myHeros != null){//To be?
					if(myHeros[i].isDestroyed){//Is destroyed Heros?
						myHeros[i] = null;
						myHeros.splice(i, 1); //Deleted elements.
					}else
						myHeros[i].evUpdate();
				}
			}
		}
		
		/**Here execute update of my class Machine.**/
		private function evUpdateMachines():void{
			for(var i:int = myMachines.length-1; i >= 0; i--){
				if(myMachines != null){//To be ??
					if(myMachines[i].isDestroyed){//Is destroyed Machine ?					
						myMachines[i] = null;
						myMachines.splice(i,1);//Deleted elements.
					}else myMachines[i].evUpdate();
				}
			}
		}
		
		/**Here execute update of my class Bullets.**/
		public function evUpdateBullet():void{
			for(var i:int = myBullets.length-1; i >= 0; i--){
				if(myBullets != null){//To be ?
					if(myBullets[i].isDestroyed){//Is destroyed Bullets?
						myBullets[i] = null;
						myBullets.splice(i, 1); //Deleted elements.
					}else myBullets[i].evUpdate();
				}
			}
		}
		
		/**Here execute update of my class Enemy**/
		private function evUpdateEnemies():void{
			for(var i:int=myEnemies.length-1; i >= 0; i--){
				if(myEnemies != null){//To be ?
					if(myEnemies[i].isDestroyed){//Is destroyed enemys?
						myEnemies[i] = null;
						myEnemies.splice(i, 1); //Deleted elements.
					}else myEnemies[i].evUpdate();
				}
			}
		}
		
		/**Here execute update of my class Powers**/
		private function evUpdatePowers():void{
			for(var i:int=myPowers.length-1; i >= 0; i--){
				if(myPowers != null){//To be ?
					if(myPowers[i].isDestroyed){//Is destroyed enemys?
						myPowers[i] = null;
						myPowers.splice(i, 1); //Deleted elements.
					}else myPowers[i].evUpdate();
				}
			}
		}

		/**This is my function spawn step by step of my timer.**/
		private function spawnPowers():void{
			  powersManager.SpawnRandom();
		}
		
		/**This is my function spawn step by step of my timer.**/
		private function spawnMachine():void{
			numMachinesSpawn ++;
			machine = new Machine;
			machine.Spawn();
			myMachines.push(machine);
		}
		
		/**This is my function spawn step by step of my timer.**/
		private function spawnEnemy():void{
			enemy = new Enemy;
			enemy.Spawn(getRandom(0,mainStage.stageWidth),1,7,3);
			
			myEnemies.push(enemy);
		}
		
		/**This function spawn step by step of my timer a Enemy2.**/
		private function spawnEnemy2():void{
			enemy = new Enemy;
			enemy.Spawn(getRandom(0,mainStage.stageWidth), 0.5, 2,1);
			
			myEnemies.push(enemy);
		}
		
		/**This function spawn step by step of my timer a Enemy3.**/
		private function spawnEnemy3():void{
			enemy = new Enemy;
			enemy.Spawn(getRandom(0,mainStage.stageWidth), 1.3, 9,5);
			
			myEnemies.push(enemy);
		}
		
		/**Take position collision and spawn feedback(this is random).**/
		public function evDamage(posX:int, posY:int):void{
			var damage:MCdamage = new MCdamage;
			isLocked = true;
			layer3.addChild(damage);
			var i:int = getRandom(0,4);
			switch(i>0){//take the position in the stage, spawn the feedback in this.
				case i==1:{
					damage.gotoAndPlay("pow");
					audioDamage1.play();//this is the SFX for this feedback
					audioDamage1.x = damage.x;
					break;
				}
					
				case i==2:{
					damage.gotoAndPlay("bam");
					audioDamage2.play();//this is the SFX for this feedback
					audioDamage2.x = damage.x;
					break;
				}
					
				case i==3:{
					damage.gotoAndPlay("zap");
					audioDamage2.play();//this is the SFX for this feedback
					audioDamage2.x = damage.x;
					break;
				}
			}
			damage.x = posX;
			damage.y = posY;
			if (damage != null)
				damage.addEventListener("unlock", evUnlock)
		}
		
		/**This function i can control the lock animation.**/
		private function evUnlock(event:Event):void{
			isLocked = false;
		}
		
		/**Take position collision and spawn feedback.**/
		private function evGoal(posX:int, posY:int):void{
			var goal:MCgoal = new MCgoal;
			layer3.addChild(goal);
			goal.x = posX;
			goal.y = posY;
			audioGoal.play();
			audioGoal.x = goal.x;
		}
		
		/**Here FX mixer heros.**/
		private function FXmixer(posX:int, posY:int):void{
			var fxMixer:MCmixerFX = new MCmixerFX;
			layer3.addChild(fxMixer);
			fxMixer.x = posX;
			fxMixer.y = posY;
			
			fxMixer.blendMode = BlendMode.LIGHTEN;
		}
		
		/**Here FX out heros**/
		private function FXmixerOut(posX:int, posY:int):void{
			var fxMixerOut:MCmixerOut = new MCmixerOut;
			layer3.addChild(fxMixerOut);
			fxMixerOut.x = posX;
			fxMixerOut.y = posY;
			
			fxMixerOut.blendMode = BlendMode.LIGHTEN;
		}
		
		public function getRandom(min:int , max:int):Number{
			return Math.random() * (max - min) + min;
		}
		
		/**Call class bullet and push in the position.**/
		public function evShoot(posX:int, posY:int, damage:int):void{
			attack1 = new Bullet;
			attack1.Spawn(posX, posY, damage);
			audioShoot.play();
			audioShoot.volume = 0.5;
			audioShoot.x = posX;
			myBullets.push(attack1);
		}
		
		public function EvSpecialShoot(posX:int, posY:int, damage:int, numBullets:int):void{
			
			for(var i:int = -numBullets/2; i<=numBullets/2; i++){
				var newBullet:Bullet = new Bullet();
				newBullet.Spawn(posX, posY, damage);
				audioShoot.play();
				newBullet.speedX = i*10; //Open bullets.
				
				var valueRotation:Number;
				if (i < 0){
					valueRotation = ((newBullet.speedY / newBullet.speedX)+90)/2;
				}else if (i > 0){
					valueRotation = ((newBullet.speedY / newBullet.speedX)-90)/2;
				}else{valueRotation = 0;}
				
				newBullet.model.rotation = valueRotation;
				Main.myBullets.push(newBullet);
			}
		}
		
		/**Take position collision and spawn feedback, the enemy send posX and PosY**/
		public function evExplotion(posX:int, posY:int, scale:Number):void{
			var explotion:MCexplotion = new MCexplotion;
			layer3.addChild(explotion);
			
			explotion.x = posX;
			explotion.y = posY;
			audioDamageEnemy.play();
			audioDamageEnemy.x = posX;
			
			explotion.scaleX = explotion.scaleY = scale;
		}
		
		/**Reset parameters for level.**/
		public function evResetLevel():void{
			if (currentLevel > 2)background.Speed = currentLevel;
			totalPoints = numMachinesSpawn = 0;
			
			life1 = 100;
			life2 = 100;
			superLife = 100;
			
			currentTimeToSpawnPowers = timeToSpawnPowers = 7000+(currentLevel*1000);
			
			currentTimeToSpawnEnemy = timeToSpawnEnemy = 3000;
			currentTimeToSpawnEnemy2 = timeToSpawnEnemy2 = 1000;
			currentTimeToSpawnEnemy3 = timeToSpawnEnemy3 = 4000;
		}
		
		/**New parameters for new level.**/
		public function evNextLevel():void{
			currentLevel += 1;
			if (currentLevel > 2)background.Speed = currentLevel;
			totalPoints = numMachinesSpawn = 0;
			goal += 5;
			
			currentTimetoSpawnMachine = timeToSpawnMachine += 1000;
			currentTimeToSpawnPowers = timeToSpawnPowers = 8000;
			
			currentTimeToSpawnEnemy = timeToSpawnEnemy = 3000;
			currentTimeToSpawnEnemy2 = timeToSpawnEnemy2 = 1000;
			currentTimeToSpawnEnemy3 = timeToSpawnEnemy3 = 4000;
		}
		
		/**Default value for re-spawn.**/
		public function evResetDefultValue():void{
			life1 = 100;
			life2 = 100;
			superLife = 100;
			
			currentLevel = 1;
			background.Speed = 2;
			totalPoints = numMachinesSpawn = 0;
			goal = 5;
			
			currentTimeToSpawnPowers = timeToSpawnPowers = 8000;
			currentTimeToSpawnEnemy = timeToSpawnEnemy = 3000;
			currentTimeToSpawnEnemy2 = timeToSpawnEnemy2 = 1000;
			currentTimeToSpawnEnemy3 = timeToSpawnEnemy3 = 4000;
		}
		
		/**Remove assets and class.**/
		public function evRemoveAll():void{
			
			for (var i_powers:int = myPowers.length-1; i_powers >= 0; i_powers--){
				if (!myPowers[i_powers].isDestroyed)myPowers[i_powers].evDestroyedOut();	
			}
			evUpdatePowers();
			
			for (var i_machines:int = myMachines.length-1; i_machines >= 0; i_machines--){
				if (!myMachines[i_machines].isDestroyed)myMachines[i_machines].evDestroyed();	
			}
			evUpdateMachines();
			
			for (var i_enemies:int = myEnemies.length-1; i_enemies >= 0; i_enemies--){
				if (!myEnemies[i_enemies].isDestroyed)myEnemies[i_enemies].evDestroyedOut();
			}
			evUpdateEnemies();
			
			for (var i_heros:int = myHeros.length-1; i_heros >= 0; i_heros--){
				if (!myHeros[i_heros].isDestroyed)myHeros[i_heros].evDestroyed();
			}
			
			if (superHero != null){
				superHero.evDestroyed();
				superHero = null;
			}
			evUpdateHeros();
			
			for (var i_bullets:int = myBullets.length-1; i_bullets >= 0; i_bullets--){
				if (!myBullets[i_bullets].isDestroyed)myBullets[i_bullets].evDestroyedOut();
			}
			evUpdateBullet();
			
			if (background != null)background.evDestroyed();		
			if (pause != null)pause = null;
			if (hud != null){
				hud.evDestroyed();
				hud = null;
			}
			
			mainStage.removeEventListener(KeyboardEvent.KEY_UP, evKeyGame);
			mainStage.removeEventListener(Event.ENTER_FRAME, evUpdate);
		}
	}
}