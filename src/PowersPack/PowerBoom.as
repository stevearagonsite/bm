package PowersPack{
	
	public final class PowerBoom extends Powers{
		
		public function PowerBoom(){
			super("Power boom ready...");
			
			var model:MCpower4 = new MCpower4;
			var posY:int = -model.height;
			var posX:int = Main.instace.getRandom(0,Main.mainStage.stageWidth);
			
			Spawn(posX, posY, model, 4);
		}
		
		public function ActivePower( numHero:int ):void{
			for (var i_enemies:int = Main.myEnemies.length-1; i_enemies >= 0; i_enemies--){
				if (Main.myEnemies[i_enemies].isDestroyed == false){
					Main.instace.evExplotion( Main.myEnemies[i_enemies].model.x , 
						Main.myEnemies[i_enemies].model.y, 0.5);
					Main.myEnemies[i_enemies].life = 0;
				}
			}
		}
		
		override public function evUpdate():void{
			super.evUpdate();
		}
	}
}