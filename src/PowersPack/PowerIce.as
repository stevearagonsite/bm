package PowersPack{
	
	public final class PowerIce extends Powers{
	
		public function PowerIce(){
			super("Power ice ready...");
			
			var model:MCpower2 = new MCpower2;
			var posY:int = -model.height;
			var posX:int = Main.instace.getRandom(0,Main.mainStage.stageWidth);
			
			Spawn(posX, posY, model, 2);
		}
		
		public function ActivePower( numHero:int ):void{
			for (var i_enemies:int = Main.myEnemies.length-1; i_enemies >= 0; i_enemies--){
				if (!Main.myEnemies[i_enemies].isDestroyed && !Main.myEnemies[i_enemies].isIce){
					Main.myEnemies[i_enemies].EvIce();
				}
			}
		}
		
		override public function evUpdate():void{
			super.evUpdate();
		}
	}
}