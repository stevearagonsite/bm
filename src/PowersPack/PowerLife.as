package PowersPack{
	
	
	public final class PowerLife extends Powers{
		
		public var addLife:int = 40;
		
		public function PowerLife(){
			super("Power life ready...");
			
			var model:MCpower3 = new MCpower3;
			var posY:int = -model.height;
			var posX:int = Main.instace.getRandom(0,Main.mainStage.stageWidth);
			
			Spawn(posX, posY, model, 3);
		}
		
		public function ActivePower( numHero:int ):void{
			trace(numHero);
			switch(numHero){
				case 0:{
					Main.myHeros[numHero].evAddLife(addLife, 1);
					break;
				}
				case 1:{
					Main.myHeros[numHero].evAddLife(addLife , 0);
					break;
				}
				case 2:{
					Main.superHero.evAddLife(addLife);
					break;
				}
			}
		}
		
		override public function evUpdate():void{
			super.evUpdate();
		}
	}
}