package PowersPack{
	
	public final class PowerShoot extends Powers{
	
		public function PowerShoot(){
			super("Power shoot ready...");
			
			var model:MCpower1 = new MCpower1;
			var posY:int = -model.height;
			var posX:int = Main.instace.getRandom(0,Main.mainStage.stageWidth);
			
			Spawn(posX, posY, model, 1);
		}
		
		public function ActivePower( numHero:int ):void{
			if (numHero < 2){
				Main.myHeros[numHero].activeSuperShoot = true;
			}else{
				Main.superHero.activeSuperShoot = true;
			}
		}
		
		override public function evUpdate():void{
			super.evUpdate();
		}
	}
}