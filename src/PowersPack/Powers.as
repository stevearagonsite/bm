package PowersPack{
	import com.greensock.TweenMax;
	
	import flash.display.MovieClip; 
	
	public class Powers{
		
		public var speed:int = 4;//Parameter for speed move (set when it is in the stage).
		public var isDestroyed:Boolean = false;//This boolean is active for instance to deleted.
		public var model:MovieClip;//Is the asset in the stage.
		
		public function Powers(text:String){trace(text);}
		
		/**Here i can push in the stage powers and a position.**/
		public function Spawn(posX:int, posY:int, model:MovieClip,power:int):void{
			this.model = model;
			Main.layer1.addChild(model);//Painting the hero in the stage.
			model.scaleX = model.scaleY = 0.5;//Scale for model.
			model.trigger.visible = false;
			model.x = posX;//Position in the stage wiht the hero.
			model.y = posY;
			evFX(power);
		}
		
		private function evFX(power:int):void{
			
			TweenMax.to(model, 1, {blurFilter:{blurY:3}});
			switch(power){
				case 1:{
					TweenMax.to(model, 1, {dropShadowFilter:{color:0xffff00, 
						alpha:0.7, blurX:50, blurY:100, strength:0.6, angle:270, distance:30}});
					break;
				}
				case 2:{
					TweenMax.to(model, 1, {dropShadowFilter:{color:0x9900cc, 
						alpha:0.7, blurX:50, blurY:100, strength:0.6, angle:270, distance:30}});
					break;
				}
				case 3:{
					TweenMax.to(model, 1, {dropShadowFilter:{color:0xff0000, 
						alpha:0.7, blurX:50, blurY:100, strength:0.6, angle:270, distance:30}});
					break;
				}
				case 4:{
					TweenMax.to(model, 1, {dropShadowFilter:{color:0x0000ff, 
						alpha:0.7, blurX:50, blurY:100, strength:0.6, angle:270, distance:30}});
					break;
				}
			}
		}
		
		/**This function is the Update only for this class**/
		public function evUpdate():void{
			if (model != null){
				model.y += speed;//Move of my bullet.
				evDestroyed();
			}
		}
		
		/**Execute this event when my bullet is out for the stage**/
		public function evDestroyed():void{
			if (model != null && !isDestroyed && model.y >= Main.mainStage.stageHeight+ model.height){
				Main.layer1.removeChild(model);
				model = null;
				isDestroyed = true
			}
		}
		
		/**Execute only for menu, no game.**/
		public function evDestroyedOut():void{
			if (model != null){
				Main.layer1.removeChild(model);
				model = null;
				isDestroyed = true
			}
		}
	}
}