package PowersPack{
	
	public class PowersManager{
		
		public var powerLife:PowerLife;
		public var powerShoot:PowerShoot;
		public var powerIce:PowerIce;
		public var powerBoom:PowerBoom;
		public var numRandom:int;
		
		public function PowersManager(){trace("Powers manager ready...");}
		
		public function SpawnRandom():void{
			EvRandomForLevel();
			EvSpawnPower();
		}
		
		private function EvRandomForLevel():void{
			switch(Main.currentLevel){
				case 1:{
					numRandom = 1;
					break;
				}
				case 2:{
					numRandom = Main.instace.getRandom(0,3);
					break;
				}
				case 3:{
					numRandom = Main.instace.getRandom(0,4);
					break;
				}
				case 4:{
					numRandom = Main.instace.getRandom(0,5);
					break;
				}
			}
		}
		
		private function EvSpawnPower():void{
			switch(numRandom){
				case 1:{
					powerShoot = new PowerShoot;
					Main.myPowers.push(powerShoot);
					break;
				}
				case 2:{
					powerIce = new PowerIce;
					Main.myPowers.push(powerIce);
					break;
				}
				case 3:{
					powerLife = new PowerLife;
					Main.myPowers.push(powerLife);
					break;
				}
				case 4:{
					powerBoom = new PowerBoom;
					Main.myPowers.push(powerBoom);
					break;
				}
			}
		}
		
		public function EvRemove():void{
		}
	}
}